import { IgniteTemplate } from "../ignite-html/ignite-template.js";

/**
 * Generates a series of years from the starting to the ending point.
 * @param {Number} from Starting year.
 * @param {Number} to Ending year.
 * @param {String} placeholder The placeholder option, it's value will be 0, if set this text will be shown.
 * @param {Boolean} descending Whether or not the to year is first and the from year is last.
 * @returns {IgniteTemplate} This ignite template.
 * 
 * @example
 * .years(1950, 2021, true) //Generates a select descending, 2021, 2020, 2019.. ect.
 */
 IgniteTemplate.prototype.years = function(from, to, placeholder = null, descending = false) {
    //Generate html and populate the template with it.
    var html = '';

    //If to is 0, null, or undefined, use the current year.
    if (to == 0 || !to) {
        to = new Date().getFullYear();
    }

    //If from is negative, adjust it with the to year.
    if (from < 0) {
        from += to;
    }

    if (placeholder) {
        html += `<option value='0'>${placeholder}</option>`;
    }

    if (descending) {
        for (var i = to; i >= from; i--) {
            html += `<option value='${i}'>${i}</option>`;
        }
    } else {
        for (var i = from; i <= to; i++) {
            html += `<option value='${i}'>${i}</option>`;
        }
    }

    return this.innerHTML(html);
};

/**
 * Generates a series of months.
 * @param {String} placeholder The placeholder option, it's value will be 0, if set this text will be shown.
 * @param {Boolean} shorthand If true the option text will be the shorthand version of the month.
 * @returns {IgniteTemplate} This ignite template.
 */
 IgniteTemplate.prototype.months = function(placeholder = null, shorthand = true) {
    //Generate html and populate the template with it.
    var html = '';

    if (placeholder) {
        html += `<option value='0'>${placeholder}</option>`;
    }

    if (shorthand) {
        html += `
        <option value='1'>Jan</option>
        <option value='2'>Feb</option>
        <option value='3'>Mar</option>
        <option value='4'>Apr</option>
        <option value='5'>May</option>
        <option value='6'>Jun</option>
        <option value='7'>Jul</option>
        <option value='8'>Aug</option>
        <option value='9'>Sep</option>
        <option value='10'>Oct</option>
        <option value='11'>Nov</option>
        <option value='12'>Dec</option>
    `;
    } else {
        html += `
        <option value='1'>January</option>
        <option value='2'>February</option>
        <option value='3'>March</option>
        <option value='4'>April</option>
        <option value='5'>May</option>
        <option value='6'>June</option>
        <option value='7'>July</option>
        <option value='8'>August</option>
        <option value='9'>September</option>
        <option value='10'>October</option>
        <option value='11'>November</option>
        <option value='12'>December</option>
    `;
    }

    return this.innerHTML(html);
}

/**
 * Generates a series of days.
 * @param {Number} from The starting day.
 * @param {Number} to The ending day.
 * @param {String} placeholder The placeholder option if set, it's value will be 0, this will be the text displayed.
 * @param {Boolean} descending Whether or not the to day is first and the from year is last.
 * @returns {IgniteTemplate} This ignite template.
 */
 IgniteTemplate.prototype.days = function(from = 1, to = 31, placeholder = null, descending = false) {
    //Generate html and populate the template with it.
    var html = '';

    if (placeholder) {
        html += `<option value='0'>${placeholder}</option>`;
    }

    if (descending) {
        for (var i = to; i >= from; i--) {
            html += `<option value='${i}'>${i}</option>`;
        }
    } else {
        for (var i = from; i <= to; i++) {
            html += `<option value='${i}'>${i}</option>`;
        }
    }

    return this.innerHTML(html);
}

/**
 * Generates a series of numbers.
 * @param {Number} from The starting number.
 * @param {Number} to The ending number.
 * @param {String} placeholder The placeholder option if set, it's value will be -1, this will be the text displayed.
 * @param {Boolean} descending Whether or not the from number if first and the to number is last.
 * @returns {IgniteTemplate} This ignite template.
 */
 IgniteTemplate.prototype.numbers = function(from = 0, to = 100, placeholder = null, descending = false) {
    //Generate html and populate the template with it.
    var html = '';

    if (placeholder) {
        html += `<option value='-1'>${placeholder}</option>`;
    }

    if (descending) {
        for (var i = to; i >= from; i--) {
            html += `<option value='${i}'>${i}</option>`;
        }
    } else {
        for (var i = from; i <= to; i++) {
            html += `<option value='${i}'>${i}</option>`;
        }
    }

    return this.innerHTML(html);
}

/**
 * Generates a series of options from an array of values, or an object of key values pairs.
 * @param {Array|Object} options An array of strings or list of key value pairs to create options from.
 * @param {*} placeholder The placeholder option, it's value will be 0, if set this text will be shown.
 * @returns {IgniteTemplate} This ignite template.
 * @example IgniteTemplate.prototype.options([{"Custom value": "Display value"}, "value2", "value3"]);
 * @example IgniteTemplate.prototype.options({"value1": "A", "value2": "B"});
 */
 IgniteTemplate.prototype.options = function(options, placeholder = null) {
    //Generate html and populate the template with it.
    var html = '';

    if (placeholder) {
        html += `<option value='-1'>${placeholder}</option>`;
    }

    if (Array.isArray(options)) {
        for (var i = 0; i < options.length; i++) {
            if (options[i] instanceof Object) {
                var keys = Object.keys(options[i]);
                html += `<option value='${keys[0]}'>${options[i][keys[0]]}</option>`;
            } else {
                html += `<option value='${options[i]}'>${options[i]}</option>`;
            }
        }
    } else {
        var keys = Object.keys(options);
        for (var i = 0; i < keys.length; i++) {
            html += `<option value='${keys[i]}'>${options[keys[i]]}</option>`;
        }
    }

    return this.innerHTML(html);
}